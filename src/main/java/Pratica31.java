
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas
 */
public class Pratica31 {
    private static Date inicio, fim;
    private static String meuNome = "Lucas Bilk Matos";
    private static GregorianCalendar dataNascimento;
    private static GregorianCalendar dataAtual;
    
    public static void main(String[] args) {
        inicio = new Date();         
                            
        String[] nomeSplit = meuNome.split(" ");
        
        dataNascimento = new GregorianCalendar(1993, 7, 18);
        dataAtual = new GregorianCalendar();
                      
        //impressão 
        System.out.println(meuNome.toUpperCase());  
        
        System.out.print(nomeSplit[nomeSplit.length - 1].substring(0,1).toUpperCase() + nomeSplit[nomeSplit.length - 1].substring(1).toLowerCase() + ",");
        for (int i = 0; i < (nomeSplit.length - 1); i++){
            System.out.print(" " + nomeSplit[i].substring(0,1).toUpperCase() + ".");
        }
        System.out.println();
        System.out.println((dataAtual.getTimeInMillis() - dataNascimento.getTimeInMillis()) / (1000*60*60*24));
        fim = new Date();
        System.out.println(fim.getTime() - inicio.getTime());
        //fim impressão
       
    }
}
